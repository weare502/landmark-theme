(function($) {
	$(document).ready(function() {
		// This function removes the hyperlink from the BarChart <script> (requested by Landmark)
		// Also need to remove the target attribute as it will open a blank tab still
		$('.compName a').attr('href', 'javascript: void(0);');
		$('.compName a').attr('target', null);
	});
})(jQuery)