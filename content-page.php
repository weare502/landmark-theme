<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Landmark National Bank
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header" >
		<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php global $post; if ( ! has_shortcode( $post->post_content, 'landmark-pricing-table' ) ){
				echo do_shortcode( '[landmark-pricing-table]' );
			} ?>
		<?php the_content(); ?>

		<?php if ( 29 == get_the_id() ) { 
			// get_template_part( 'careers', 'list' ); 
		} ?>
		<?php if ( 266 == get_the_id() ) { 
			get_template_part( 'faq', 'list' ); 
		} ?>
		<?php if ( 328 == get_the_id() ) { ?>
			<script type="text/javascript" src="https://www.barchart.com/widget.js?uid=6418e8a2b101a6f230246c0db8e0bfc7&widgetType=singlequote&widgetWidth=300&primaryPrice=last&fontColor%5Blinks%5D=AD2162&font=1&symbol=LARK&fields%5B%5D=name&fields%5B%5D=symbol&fields%5B%5D=last&fields%5B%5D=change&fields%5B%5D=pctchange&fields%5B%5D=displaytime&displayChars="></script>
		<?php } ?>

		<?php if ( have_rows( 'additional_services_links' ) ) : ?>

			<div class="additional-services clearfix">

			<?php while ( have_rows( 'additional_services_links' ) ) : the_row(); ?>

				<div class="service">

				<?php 
					$link = get_sub_field('link');
					$speedbump = '';
					if ( get_sub_field('external') ) {
						$link = get_sub_field('external_link');
						$speedbump = get_sub_field('speed_bump');
					}

				 ?>

					<a href="<?php echo $link; ?>" class="<?php echo $speedbump; ?> ">

						<span class="fa-stack fa-lg">
						  <span class="fa fa-circle fa-stack-2x"></span>
						  <span class="fa <?php the_sub_field( 'icon' ); ?> fa-stack-1x"></span>
						</span>

						<div class="title">
							<?php the_sub_field( 'title' ); ?>
						</div>

						<div class="description">
							<?php the_sub_field( 'description' ); ?>
						</div>

					</a>

				</div>

			<?php endwhile; ?>

			</div>

		<?php endif; ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'lnb' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
