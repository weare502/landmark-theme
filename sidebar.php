<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Landmark National Bank
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary" aria-label="Primary Sidebar" itemscope itemtype="https://schema.org/WPSideBar">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php 
		if ( is_page() ) :

			global $post;
			$sidebar_image = get_field('sidebar_image', $post->ID );
			$sidebar_link_image = get_field( 'sidebar_link_image', $post->ID );
			$sidebar_image_link = '';
			if ( 'external' == $sidebar_link_image ){
				$sidebar_image_link = get_field( 'sidebar_external_link' );
			} else if ( 'internal' == $sidebar_link_image ){
				$sidebar_image_link = get_field( 'sidebar_internal_link' );
			}
			
			if ( $sidebar_image ) : ?>

				<?php if ( ! empty( $sidebar_image ) ){
					echo "<a href='{$sidebar_image_link}' class='sidebar-link " . ( 'external' == $sidebar_link_image ? get_field( 'sidebar_speed_bump' ) : '' ) . "'>";
				} ?>
				
				<img src="<?php echo $sidebar_image['url']; ?>" class="sidebar-image" title="<?php echo $sidebar_image['title'] ?>" alt="<?php echo $sidebar_image['alt'] ?>" />

				<?php if ( ! empty( $sidebar_image ) ){
					echo "</a>";
				} ?>

			<?php endif;

		endif;
	?>
</div><!-- #secondary -->
