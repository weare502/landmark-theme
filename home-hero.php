<section class="hero top clearfix" aria-label="image slides">

	<?php 

		$slides = get_field('home_page_slides');

		if ( have_rows( 'home_page_slides' ) ) : ?>

		<div class="wrap slick-carousel" id="slick-carousel">

			<?php while ( have_rows( 'home_page_slides' ) ) : the_row();

				$banner =  get_sub_field( 'home_page_banner_image' ); // var_dump($banner); 
				$link = get_sub_field( 'home_page_banner_link' );

				$link_before = '';
				$link_after  = '';

				if ( ! empty( $link ) ){
					$link_before = "<a href='{$link}' class='banner-link'>";
					$link_after = "</a>";
				}
			?>

			 <div class="item"><?php echo $link_before; ?><img src="<?php echo $banner['url']; ?>" class="banner" width="<?php echo $banner['width']; ?>" height="<?php echo $banner['height']; ?>" alt="<?php echo $banner['alt']; ?>" title="<?php echo $banner['title']; ?>" /><?php echo $link_after; ?></div>
			
			<?php endwhile; ?>

		</div>

	<?php endif; ?>

</section>

<section class="hero clearfix" aria-label="Quick Links">

	<div class="wrap clearfix">

		<div class="quick-links clearfix">
				<?php the_widget( 'Landmark_Quick_Links', array() ,array( 'before_widget' => '', 'after_widget' => '', 'before_title' => '<span class="screen-reader-text">', 'after_title' => '</span>' ) ); ?>
		</div>

		<div class="call-outs clearfix">

			<div class="call-out">
				
				<?php if ( ! get_field('call_out_1_is_external_link' ) ) : ?>
					<a href="<?php the_field( 'call_out_1_link' ); ?>">
				<?php else : ?>
					<a href="<?php the_field( 'call_out_1_external_link' ); ?>" target="_blank" class="outgoing">
				<?php endif; ?>
					
					<span class="fa-stack fa-lg">
					  <span class="fa fa-circle fa-stack-2x"></span>
					  <span class="fa <?php the_field('call_out_1_icon'); ?> fa-stack-1x"></span>
					</span>

					<div class="title"><?php the_field( 'call_out_1_title' ); ?></div>
					<div class="text"><?php the_field( 'call_out_1_text' ); ?></div>
				</a>
					
			</div>

			<div class="call-out">

				<?php if ( ! get_field('call_out_2_is_external_link' ) ) : ?>
					<a href="<?php the_field( 'call_out_2_link' ); ?>">
				<?php else : ?>
					<a href="<?php the_field( 'call_out_2_external_link' ); ?>" target="_blank" class="outgoing">
				<?php endif; ?>
					
					<span class="fa-stack fa-lg">
					  <span class="fa fa-circle fa-stack-2x"></span>
					  <span class="fa <?php the_field('call_out_2_icon'); ?> fa-stack-1x"></span>
					</span>

					<div class="title"><?php the_field( 'call_out_2_title' ); ?></div>
					<div class="text"><?php the_field( 'call_out_2_text' ); ?></div>
				</a>
					
			</div>

			<div class="call-out">
				
				<?php if ( ! get_field('call_out_3_is_external_link' ) ) : ?>
					<a href="<?php the_field( 'call_out_3_link' ); ?>">
				<?php else : ?>
					<a href="<?php the_field( 'call_out_3_external_link' ); ?>" target="_blank" class="outgoing">
				<?php endif; ?>
					
					<span class="fa-stack fa-lg">
					  <span class="fa fa-circle fa-stack-2x"></span>
					  <span class="fa <?php the_field('call_out_3_icon'); ?> fa-stack-1x"></span>
					</span>

					<div class="title"><?php the_field( 'call_out_3_title' ); ?></div>
					<div class="text"><?php the_field( 'call_out_3_text' ); ?></div>
				</a>
					
			</div>
		
		</div>

	</div>
</section>