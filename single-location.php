<?php
/**
 * The template for displaying all single posts.
 *
 * @package Landmark National Bank
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php if ( function_exists('yoast_breadcrumb') ) {
				$breadcrumb = yoast_breadcrumb('<p id="breadcrumbs">','</p>', false);

				$breadcrumb = str_replace('/location/', '/locations/', $breadcrumb);

				if ( function_exists('em_get_location') ) $breadcrumb = str_replace('/events/', '/', $breadcrumb);
				
				echo $breadcrumb;
			} ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<div class="entry-content">

				<?php $location = function_exists('em_get_location') ? em_get_location( $post->ID, 'post_id' ) : ''; 
				// var_dump( $location->output( '#_LOCATIONMAP' ) ); // ADD A MAP or Other Output ?>

					<button id="view-on-google"><?php _e( 'View on Google Maps'); ?></button>
					<?php the_content(); ?>
					<p><strong><?php _e( 'Phone Number: ', 'lnb' ); ?></strong><a href="tel:<?php the_field('location_phone_number'); ?>"><?php the_field( 'location_phone_number' ); ?></a></p>
					<p><strong><?php _e('ATM: ', 'lnb'); ?></strong>
						<?php if ( true === get_field('location_atm') ) : ?>
							<span class="fa fa-check"><span class="screen-reader-text">Yes</span></span>
						<?php else : ?>
							<span class="fa fa-times-circle-o"><span class="screen-reader-text">No</span></span>
						<?php endif; ?>
					</p>
					<p><strong><?php _e( 'Lobby Hours', 'lnb' ); ?></strong><br/><?php the_field('location_lobby_hours'); ?></p>
					<p><strong><?php _e( 'Drive-Up Hours', 'lnb' ); ?></strong><br/><?php the_field('location_drive_up_hours'); ?></p>

					<!-- <p><strong><?php _e( 'Upcoming Events at this Location', 'lnb' ); ?></strong><br/><?php echo function_exists('em_get_location') ? $location->output( '#_LOCATIONNEXTEVENTS' ) : __( 'No event information is available at this time.', 'lnb' ); ?></p> -->
				</div><!-- .entry-content -->

				<footer class="entry-footer">
					<?php lnb_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</article><!-- #post-## -->

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>

<script type="text/javascript">
	(function( $ ){ 

		var poBox = $('.wpsl-location-address').html();
		poBox = poBox.replace( 'PO B', '<br>PO B' );

		$('.wpsl-location-address').html( poBox );

	})(jQuery);

</script>
<?php get_footer(); ?>