<?php

$labels = array(
	'name'               => __( 'Loan Officers', 'lnb' ),
	'singular_name'      => __( 'Loan Officer', 'lnb' ),
	'add_new'            => _x( 'Add New Loan Officer', 'lnb', 'lnb' ),
	'add_new_item'       => __( 'Add New Loan Officer', 'lnb' ),
	'edit_item'          => __( 'Edit Loan Officer', 'lnb' ),
	'new_item'           => __( 'New Loan Officer', 'lnb' ),
	'view_item'          => __( 'View Loan Officer', 'lnb' ),
	'search_items'       => __( 'Search Loan Officers', 'lnb' ),
	'not_found'          => __( 'No Loan Officers found', 'lnb' ),
	'not_found_in_trash' => __( 'No Loan Officers found in Trash', 'lnb' ),
	'parent_item_colon'  => __( 'Parent Loan Officer:', 'lnb' ),
	'menu_name'          => __( 'Loan Officers', 'lnb' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => true,
	'description'         => '',
	'taxonomies'          => array( 'region' ),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'show_in_rest'		  => true,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-universal-access',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => array( 'slug' => 'commercial-loan-officer' ),
	'capability_type'     => 'post',
	'supports'            => array( 'title', 'editor' ),
);

register_post_type( 'loan_officer', $args );