<?php if( have_rows( 'career_openings' ) ) : ?>

	<div class="career-openings">

	<h2><?php _e( 'Current Job Openings', 'lnb' ); ?></h2>

 	<?php while ( have_rows( 'career_openings' ) ) : the_row(); ?>

 		<?php $locations = get_sub_field( 'location' );

 		$locations = empty( $locations ) ? __('All Locations', 'lnb') : $locations;

 		$file = get_sub_field( 'file' );

 		$app_url = get_permalink( 175 );

 		$app_url = add_query_arg( 'job_position', urlencode( get_sub_field('title') ), $app_url );

   		?>

   		<div class="opening">

	        <h3 class="job-title"><span class="fa fa-caret-right">&nbsp;</span><span><?php the_sub_field( 'title' ); ?></span></h3>

	        <section class="details clearfix" aria-label="Job Opening">

		       	<p class="location">
		      		<strong><?php _e('Locations: ', 'lnb' ); ?></strong>
		       		<?php if ( is_array( $locations ) ) {
		       			
			       		$last = count( $locations );
			       		$i = 0;
		       			foreach ( $locations as $location ){
		       				$i++;
		       				echo "<a href='" . get_permalink( $location ) . "'>" . get_the_title( $location ) . "</a>" ;
		       				if ( $i < $last )
		       					echo ", ";
		       			}
		       		} else {
		       			echo "<a href='" . get_permalink( 84 ) . "'>" . $locations . "</a>";
		       		} ?>
		       		<br/>
		       		<strong><?php _e('Hours: '); ?></strong><?php the_sub_field( 'hours' ); ?>
		       	</p>

		        <p>

		        	<?php the_sub_field( 'short_job_description' ); ?>

			        <?php if ( ! empty( $file ) ) : ?>
			        	<br/><br/><strong><?php _e( 'More information: ', 'lnb' ); ?></strong><a href="<?php echo $file['url']; ?>"><?php _e( 'Download File' ); ?></a>
			        <?php endif; ?>

		        </p>

		        <?php if ( is_array( $locations ) ) {

			        foreach ( $locations as $location ){
			        	$app_url = add_query_arg( urlencode( 'locations[]' ), urlencode( get_the_title( $location ) ), $app_url );
			        }
			        $app_url = esc_url( $app_url );

			    } else {

			    	$locations = get_posts(array( 'post_type' => 'location', 'posts_per_page' => -1, 'orderby' => 'name', 'order' => 'ASC' ) );
			    	foreach ( $locations as $location ){
			    		$app_url = add_query_arg( urlencode( 'locations[]' ), urlencode( get_the_title( $location->ID ) ), $app_url );
			    	}
			    	$app_url = esc_url( $app_url );

			    } ?>

		        <a href="<?php echo $app_url; ?>" class="apply"><?php _e( 'Apply For This Position', 'lnb' ); ?></a>

	        </section>

        </div>

    <?php endwhile; ?>

    </div>

    <?php else : ?>

    <p><?php _e( "We're sorry, but there are currently no job openings available. Please check back soon." , 'lnb' ); ?></p>

<?php endif; ?>

<script>

jQuery( document ).ready( function( $ ){

	$titles = $( '.job-title' );

	$( $titles ).on( 'click', function(){
		 $( this ).siblings( '.details' ).slideToggle( 200 );
		 $( this ).toggleClass( 'toggled' );
	} );

} );

</script>

<noscript>
	<!-- JS is Disabled -->
	<style type="text/css">
		.details {
			display: block !important;
		}
	</style>
</noscript>

<?php // end of file ?>