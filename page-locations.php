<?php
/**
 * The template for displaying the Locations page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Landmark National Bank
 */

$args = array(
	'post_type' => 'location',
	'posts_per_page' => -1,
	'orderby' => 'name',
	'order' => 'ASC'
);

$locations = new WP_Query( $args );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php if ( function_exists('yoast_breadcrumb') ) {
					$breadcrumb = yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>

						<div class="location-maps clearfix">

							<ul class="show-tabs">
								<li class="map"><a href="#"><?php _e( 'Map', 'lnb' ); ?></a></li>
								<li class="search"><a href="#"><?php _e( 'Search', 'lnb' );?></a></li>
								<li class="all-locations"><a href="#"><?php _e( 'All Locations', 'lnb' ); ?></a></li>
							</ul>

							<section class="map tab" aria-label="change to map tab">
								<?php echo do_shortcode( '[simple_locator_all_locations mapcontrols=”hide” mapheight="500"]' ); ?>
							</section>

							<section class="search tab" aria-label="change to search locations tab">
								<?php echo do_shortcode( '[wp_simple_locator mapcontainer="#searchmap" distances="25,50,100" addresslabel="' . __('City / Zip', 'lnb') . '"]' ); ?>
								<script type="text/javascript">
									jQuery('.simple-locator-form .distance select').attr('id', 'distance');
									jQuery('.simple-locator-form .address.wpsl-search-form').attr('id', 'zip');
								</script>
								<div id="searchmap"></div>
							</section>

							<section class="all-locations tab" aria-label="change to all locations tab">
								<table>
									<thead>
										<tr>
											<th><?php _e( 'City' , 'lnb' ); ?></th>
											<th><?php _e( 'Address/Phone' , 'lnb' ); ?></th>
											<th><?php _e( 'ATM' , 'lnb' ); ?></th>
											<th><?php _e( 'Lobby Hours' , 'lnb' ); ?></th>
											<th><?php _e( 'Drive-Up Hours' , 'lnb' ); ?></th>
										</tr>
									</thead>
									<tbody>
								<?php while ( $locations->have_posts()  ) : $locations->the_post(); ?>
									
									<tr class="location">
										<td class="name"><h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></td>
										<td class="address">
											<?php $address = get_post_meta( get_the_id(), 'wpsl_address', true); echo str_replace( 'PO B', '<br/>PO B', $address); ?><br/>
											<?php echo get_post_meta( get_the_id(), 'wpsl_city', true); ?>,
											<?php echo get_post_meta( get_the_id(), 'wpsl_state', true); ?>
											<?php echo get_post_meta( get_the_id(), 'wpsl_zip', true); ?>

											<div><span class="fa fa-phone">&nbsp;<span class="screen-reader-text"><?php _e('Phone Number', 'lnb'); ?></span></span><a href="tel:<?php the_field( 'location_phone_number' ); ?>"><?php the_field( 'location_phone_number' ); ?></a></div>
										</td>
										<td class="atm">
											<?php if ( true === get_field('location_atm') ) : ?>
												<span class="fa fa-check"><span class="screen-reader-text"><?php _e( 'Yes', 'lnb' ); ?></span></span>
											<?php else : ?>
												<span class="fa fa-times-circle-o"><span class="screen-reader-text"><?php _e( 'No', 'lnb' ); ?></span></span>
											<?php endif; ?>
										</td>
										<td class="lobby"><?php the_field( 'location_lobby_hours' ); ?></td>
										<td class="drive-up"><?php the_field( 'location_drive_up_hours' ); ?></td>
									</tr>

								<?php endwhile; wp_reset_postdata(); // Reset Loop ?>

									</tbody>
								</table>
							</section>

						</div>
					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'lnb' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>

<script>
	jQuery(document).ready( function( $ ){

		var $tabs = $('.location-maps .show-tabs a');

		$( $tabs ).on('click', function( e ){

			e.preventDefault();

			var $selector = $( this ).parent('li').prop('class');

			$( $tabs ).removeClass('current');

			$( this ).addClass('current');

			$('section.tab').hide();

			$( 'section.tab.'+$selector ).toggle();

		} );

	} );


	function loadmap()
	{
		var locations = wpsl_locator_all.locations;
		var mapstyles = wpsl_locator.mapstyles;	
		var mappin = ( wpsl_locator.mappin ) ? wpsl_locator.mappin : '';
		var bounds = new google.maps.LatLngBounds();
		var mapOptions = {
				mapTypeId: 'roadmap',
				mapTypeControl: false,
				zoom: 8,
				styles: mapstyles,
				panControl : false,
				scrollwheel: true,
			//	draggable: false
			}
		var infoWindow = new google.maps.InfoWindow(), marker, i;
		var map = new google.maps.Map( document.getElementById('alllocationsmap'), mapOptions );

		var markers = [];
		
		// Loop through array of markers & place each one on the map  
		for( i = 0; i < locations.length; i++ ) {
			var position = new google.maps.LatLng(locations[i].latitude, locations[i].longitude);
			bounds.extend(position);
			
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				title: locations[i].title,
				icon: mappin
			});	
			markers.push(marker);

			// Info window for each marker 
			google.maps.event.addListener(marker, 'click', (function(marker, i){
				return function() {
					infoWindow.setContent('<h4>' + locations[i].title + '</h4><p><a href="' + locations[i].permalink + '">' + wpsl_locator.viewlocation + '</a></p>');
					infoWindow.open(map, marker);
				}
			})(marker, i));
			
			// Center the Map
			map.fitBounds(bounds);
			var listener = google.maps.event.addListener(map, "idle", function() { 
					if ( locations.length < 2 ) {
					map.setZoom(13);
				}
				google.maps.event.removeListener(listener); 
			});
		}

		// var mcOptions = {gridSize: 50, maxZoom: 15};
		// var mc = new MarkerClusterer(map, markers, mcOptions);

		// mc.redraw();

		// Fit the map bounds to all the pins
		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			google.maps.event.removeListener(boundsListener);
		});

	} // loadmap()

	jQuery( window ).load( loadmap ); // reload map to disable scrollwheel

</script>

<?php get_footer(); ?>
