<?php
/**
 * @package Landmark National Bank
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php lnb_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( is_home() ) : ?>

			<?php the_excerpt(); ?>
			The excerpt

		<?php else : ?>

			<?php if ( ! has_shortcode( $post->post_content, 'landmark-pricing-table' ) ){
					echo do_shortcode( '[landmark-pricing-table]' );
				} ?>
			<?php the_content(); ?>

			The content

		<?php endif; ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'lnb' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php lnb_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
