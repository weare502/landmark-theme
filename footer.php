<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Landmark National Bank
 */
?>

		</div><!-- #content -->

	</div><!-- .wrap -->

	<div class="wrap clearfix">

		<footer id="colophon" class="site-footer" itemscope itemtype="https://schema.org/WPFooter">
			<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' ) ); ?>
			<div class="site-info">
				<p>
					<img src="<?php echo get_template_directory_uri(); ?>/img/equal-housing.png" class="equal-housing" alt="Equal Housing Lender" />
					<img src="<?php echo get_template_directory_uri(); ?>/img/fdic.png" class="fdic" alt="Member FDIC" />
					<?php echo "&copy;" . date(" Y ") . __( 'Landmark National Bank', 'lnb' ); ?>
					<span class="social-links">
						<a href="<?php the_field('facebook_url', 'options'); ?>" class=" <?php the_field('fb_speed_bump', 'options'); ?> ">
							<?php include( get_template_directory() . "/img/facebook.svg" ); ?>
						</a>
						<a href="<?php the_field('twitter_url', 'options'); ?>" class=" <?php the_field('twitter_speed_bump', 'options'); ?> ">>
							<?php include( get_template_directory() . "/img/twitter.svg" ); ?>
						</a>
					</span>
				</p>
			</div><!-- .site-info -->
			<?php echo do_shortcode('[google-translator]'); ?>
		</footer><!-- #colophon -->

	</div><!-- .wrap -->

	</div><!-- .canvas-wrap -->

</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>