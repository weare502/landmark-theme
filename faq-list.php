<div class="frequenly-asked-questions-list">

	<div id="faq-top"></div>

	<div class="questions">

		<?php if ( have_rows( 'qs_and_as_section' ) ) :

			echo '<ol>';

			while ( have_rows( 'qs_and_as_section' ) ) : the_row();

				$question = get_sub_field('section_title');

				$link = sanitize_title( get_sub_field('section_title')  );

				echo "<li><a href='#{$link}'>{$question}</a></li>";

			endwhile;

			echo '</ol>';

		endif; ?>

	</div>

	<div class="answers">

		<?php

		 if ( have_rows( 'qs_and_as_section' ) ) :

		 	while ( have_rows( 'qs_and_as_section' ) ) : the_row();

		 		$link = sanitize_title( get_sub_field('section_title')  );

		 		echo "<div class='section-title' id='{$link}'>" . get_sub_field('section_title') . "</div>";

				 if ( have_rows( 'qs_and_as' ) ) :

					while ( have_rows( 'qs_and_as' ) ) : the_row();

						$question = get_sub_field('question');

						$answer = get_sub_field('answer');

						$link = sanitize_title( get_sub_field('question')  );

						echo "<div class='question-wrap'><p class='question' id='{$link}'><span class='fa-stack fa-lg'><span class='fa fa-circle fa-stack-2x'></span><span class='fa fa-question fa-stack-1x fa-inverse'></span></span>{$question}</p>";

						echo $answer;

						echo "<a href='#faq-top' class='fa fa-angle-up'> " . __('Back to Questions') . "</a></div>";

					endwhile;

				endif; 

			endwhile;

		endif; ?>

	</div>
</div>