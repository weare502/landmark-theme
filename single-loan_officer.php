<?php
/**
 * Template for displaying individual employee pages
 *
 * @package Landmark National Bank
 */

get_header(); ?>

	<?php // acf vars
		$certs = get_field( 'employee_certifications' );
		$email = get_field( 'employee_email' );
		$phone = get_field( 'employee_phone' );
		$addx_one = get_field( 'bank_street_address' );
		$addx_two = get_field( 'street_address_two' );
		$city = get_field( 'city' );
		$zip = get_field( 'zip_code' );
		$linkedin = get_field( 'linkedin_profile_url' );

		$review = get_field( 'review' );
		$review_name = get_field( 'reviewer_name' );

		$about = get_field( 'employee_about' );
		$why_lm = get_field( 'why_landmark' );
		$video_url = get_field('lo_video_embed');
	?>

	<section class="officer-intro-header">
		<div class="officer-header-info">
			<div class="info-box-img">
				<img src="<?php echo get_field( 'employee_image', $post->ID )['url']; ?>" alt="<?php echo get_field('employee_image', $post->ID )['alt']; ?>">
			</div>

			<div class="info-box-text">
				<span class="info-title"><?php the_title(); ?> </span>
				<span class="info-title"> <?php echo $certs; ?></span>
				<?php while( have_posts() ) : the_post(); the_content(); endwhile; ?>
			</div>

			<div class="info-box-contact">
				<h3>Contact</h3>
				<a class="lo-email" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a><br>
				<a class="add-space" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?><a>
				<p class="no-marg"><?php echo $addx_one; ?></p>
				<p class="no-marg"><?php echo $addx_two; ?></p>
				<p class="no-marg"><?php echo $city; ?>, KS &nbsp; <?php echo $zip; ?></p>
				<a target="_blank" href="<?php echo $linkedin; ?>" title="Linkedin Icon">
					<img class="social-linkedin" src="<?php echo get_template_directory_uri(); ?>/img/linkedin.jpg">
				</a>
			</div> <!-- /.[info-box-contact] -->
		</div>
	</section> <!-- /.[officer-intro-header] -->

	<section class="officer-main-info">
		<div class="flex-container">

			<aside class="flex-left">
				<h3>Specialties</h3>
				<?php if( have_rows( 'job_specialties' ) ) :
					while( have_rows( 'job_specialties' ) ) : the_row();

					$specialty = get_sub_field( 'employee_specialties' );
					?>

					<p><?php echo $specialty; ?></p>
				<?php endwhile; endif; ?>

				<?php if($review) : ?>
					<div class="review hide-mobile">
						<p><?php echo $review; ?></p>
						<p class="reviewer-name"><?php echo $review_name; ?></p>
					</div>
				<?php endif; ?>
			</aside>

			<div class="flex-right">
				<h3>Education</h3>
				<?php if( have_rows( 'employee_education' ) ) :
					while( have_rows( 'employee_education' ) ) : the_row();

					$education = get_sub_field( 'degree_or_university' );
					?>

					<p class="last"><?php echo $education; ?></p>
				<?php endwhile; endif; ?>

				<h3>Personal</h3>
				<p><?php echo $about; ?></p>

				<h3>Why Landmark?</h3>
				<p style="padding-bottom: 2rem;"><?php echo $why_lm; ?></p>
				
				<?php if($video_url) : ?>
					<iframe src="https://www.youtube.com/embed/<?php echo $video_url; ?>" frameborder="0" allowfullscreen></iframe>
				<?php endif; ?>

			</div>
		</div> <!-- /.[flex-container] -->
	</section>

<?php get_footer(); ?>