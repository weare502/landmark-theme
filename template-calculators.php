<?php
/**
 * Template Name: Calculators
 */

$calc_dir = get_template_directory_uri() . '/calculators';
$perm = get_permalink( get_the_id() );
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php if ( function_exists('yoast_breadcrumb') ) {
					$breadcrumb = yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php global $post; if ( ! has_shortcode( $post->post_content, 'landmark-pricing-table' ) ){
								echo do_shortcode( '[landmark-pricing-table]' );
							} ?>
						<?php the_content(); ?>

						<p><?php _e( 'List of Available Calculators', 'lnb' ); ?></p>
						
						<ul>
							<li><a href="<?php echo $perm; ?>?calc=CertDeposit"><?php _e( 'Certificate of Deposit Calculator', 'lnb'); ?></a></li>
							<li><a href="<?php echo $perm; ?>?calc=CheckBook"><?php _e( 'Checkbook Balancer', 'lnb'); ?></a></li>
							<li><a href="<?php echo $perm; ?>?calc=CompoundSavings"><?php _e( 'Savings Calculator', 'lnb'); ?></a></li>
							<li><a href="<?php echo $perm; ?>?calc=EnhancedLoan"><?php _e( 'Enhanced Loan Calculator', 'lnb' ); ?></a></li>
							<li><a href="<?php echo $perm; ?>?calc=RothvsRegular"><?php _e( 'Roth IRA vs. Traditional IRA', 'lnb' ); ?></a></li>
						</ul>

						<?php if ( isset( $_GET['calc'] ) && !empty( $_GET['calc'] ) ) : ?>

							<link type='text/css' rel='StyleSheet' href='<?php echo $calc_dir; ?>/KJE.css' /> 
							<link type='text/css' rel='StyleSheet' href='<?php echo $calc_dir; ?>/KJESiteSpecific.css' /> 
							<noscript><div align=center><div align=center id="KJENoJavaScript" class="KJENoJavaScript">Javascript is required for this calculator.  If you are using Internet Explorer, you may need to select to 'Allow Blocked Content' to view this calculator.</div></div></noscript>

							<div id="KJEAllContent"></div>

							<!--[if lt IE 9]><script language="JavaScript" SRC="<?php echo $calc_dir; ?>/excanvas.js"></script><![endif]-->
							<script language="JavaScript" type="text/javascript" SRC="<?php echo $calc_dir; ?>/KJE.js"></script>
							<script language="JavaScript" type="text/javascript" SRC="<?php echo $calc_dir; ?>/KJESiteSpecific.js"></script> 
							<script language="JavaScript" type="text/javascript" SRC="<?php echo $calc_dir . '/' . $_GET['calc']; ?>.js"></script>
							<script language="JavaScript" type="text/javascript" SRC="<?php echo $calc_dir . '/' . $_GET['calc']; ?>Params.js"></script>

						<?php endif; ?>

					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'lnb' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->


				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

