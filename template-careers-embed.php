<?php
/**
 * Template Name: Careers Embed
 */

$perm = get_permalink( get_the_id() );
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php if ( function_exists('yoast_breadcrumb') ) {
					$breadcrumb = yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				} ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php global $post; if ( ! has_shortcode( $post->post_content, 'landmark-pricing-table' ) ){
								echo do_shortcode( '[landmark-pricing-table]' );
							} ?>
						<?php the_content(); ?>

						<div style="height: 1300px; -webkit-overflow-scrolling:touch;">
							<iframe src="https://www.paycomonline.net/v4/ats/web.php/jobs?clientkey=0B77FA75463C9E18602AE4279744FEFE" style="width: 100%; height: 100%; border: none;"></iframe>
						</div>

					</div><!-- .entry-content -->

					<footer class="entry-footer">
						<?php edit_post_link( __( 'Edit', 'lnb' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-footer -->
				</article><!-- #post-## -->

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>