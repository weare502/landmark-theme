


  KJE.parameters.set("NEW_LOAN_BALANCE",250000);
  KJE.parameters.set("NEW_LOAN_PAYMENT",100);
  KJE.parameters.set("NEW_LOAN_RATE",6.9);
  KJE.parameters.set("NEW_LOAN_TERM",30);
  KJE.parameters.set("CALC_INDEX",KJE.EnhancedLoanCalc.CALC_PAYMENT);




/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, ©1998-2017 KJE Computer Solutions, LLC.
  For more information please see:
  <A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>
 -->
 */
if (KJE.IE7and8) KJE.init();

