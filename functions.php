<?php
/**
 * Landmark National Bank functions and definitions
 *
 * @package Landmark National Bank
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1200; /* pixels */
}

if ( ! function_exists( 'lnb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function lnb_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Landmark National Bank, use a find and replace
	 * to change 'lnb' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'lnb', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'lnb' ),
		'footer'  => __( 'Footer Menu' , 'lnb' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	// add_theme_support( 'post-formats', array(
	// 	'aside', 'image', 'video', 'quote', 'link',
	// ) );

	// Set up the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'lnb_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );
}
endif; // lnb_setup
add_action( 'after_setup_theme', 'lnb_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function lnb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'lnb' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'lnb_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function lnb_scripts() {
	wp_enqueue_style( 'lnb-style', get_stylesheet_uri(), false, '20150713' );

	wp_enqueue_style( 'lnb-source-sans-pro', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700:latin' );

	wp_enqueue_script( 'lnb-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'lnb-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'lnb-customjs', get_template_directory_uri() . '/js/site-cstm.js', array( 'jquery' ), '20120207', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_page( 13 ) ){
		wp_enqueue_script( 'thickbox' );
	}

	if ( is_page(84) || is_singular('location') ){
		wp_enqueue_script( 'google-maps', 'https://maps.google.com/maps/api/js?key=AIzaSyAjPQxijtQ5OQSEpK5zsbdLOEH4RBz1frs&callback=initMap' );
		wp_enqueue_script( 'gmaps-markerclusters', get_template_directory_uri() . '/js/markerclusterer.js', array('google-maps'), '20120206', true );
	}

	if ( is_front_page() ){
		// wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/owl-carousel/owl.carousel.css' );
		// wp_enqueue_style( 'owl-carousel-theme', get_template_directory_uri() . '/owl-carousel/owl.theme.css' );
		// wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/owl-carousel/owl.carousel.min.js', array( 'jquery' ) );

		wp_enqueue_script( 'slick-carousel-js', get_template_directory_uri() . '/js/min/slick.min.js', array( 'jquery' ) );
	}
}
add_action( 'wp_enqueue_scripts', 'lnb_scripts' );

add_action( 'admin_enqueue_scripts', function(){

		wp_enqueue_script( 'google-maps-admin', 'https://maps.google.com/maps/api/js?sensor=false', 2 );
		
});



//key=AIzaSyAjPQxijtQ5OQSEpK5zsbdLOEH4RBz1frs&callback=initMap

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_filter( 'walker_nav_menu_start_el', function( $output, $item, $depth, $args){ 

	//var_dump( $args );

	if ( 'primary' != $args->theme_location || 0 != $depth )
		return $output;

	$output .= "<button class='nav-show-sub-menu'><span class='fa fa-chevron-down'></span><span class='screen-reader-text'>Show Sub Menu</span></button>";

	return $output;

}, 10, 4 );

add_action( 'admin_init', function(){
	add_editor_style();
});

add_action( 'wp_footer', function(){

	if ( is_page( 13 ) ) : add_thickbox(); ?>
	
		<script type="text/javascript">
			jQuery('.gallery .gallery-item a').addClass('thickbox').prop('rel', 'gallery' );
		</script>

	<?php endif; 

} );



function is_ipad() {
	$is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad');
	if ($is_ipad)
		return true;
	else return false;
}

function is_iphone() {
	$cn_is_iphone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPhone');
	if ($cn_is_iphone)
		return true;
	else return false;
}

function is_ipod() {
	$cn_is_iphone = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPod');
	if ($cn_is_iphone)
		return true;
	else return false;
}

function is_ios() {
	if (is_iphone() || is_ipad() || is_ipod())
		return true;
	else return false;
}

add_filter('body_class', function($classes) {

	if ( true === is_ios() )
		$classes[] = 'os-ios';

	return $classes;

}, 10, 1 );


add_filter( 'login_headerurl', function( $url ){ 

	$url = 'https://banklandmark.com';

	return $url;

} , 99 );	

add_filter( 'login_headertitle', function( $title ){

	$title = 'Landmark National Bank';

	return $title;

}, 99 );

add_action( 'login_head', function(){ ?>

	<style type="text/css">

		#login h1 a {
			background-image: url("<?php echo get_template_directory_uri(); ?>/img/logo.png");
			width: 100%;
			background-size: 100%;
		}

	</style>

<?php }, 99);

// Register cpt's here
add_action( 'init', 'register_post_types' );
function register_post_types() {
	require 'cpt/post-type-loan-officer.php';
}

// Register new Taxonomy for the 'Loan Officer' cpt
function loan_officer_taxonomy() {
	$labels = array(
		'name' => _x( 'Regions', 'lnb' ),
		'singular_name' => _x( 'Region', 'lnb' ),
		'search_items' => __( 'Search Regions' ),
		'all_items' => __( 'All Regions' ),
		'parent_item' => __( 'Parent Region' ),
		'parent_item_colon' => __( 'Parent Region:' ),
		'edit_item' => __( 'Edit Region' ),
		'update_item' => __( 'Update Region' ),
		'add_new_item' => __( 'Add New Region' ),
		'new_item_name' => __( 'New Region Name' ),
		'menu_name' => __( 'Regions' ),
	);

	$args = array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
	);

	register_taxonomy( 'region', 'loan_officer', $args );
}
add_action( 'init', 'loan_officer_taxonomy', 0);