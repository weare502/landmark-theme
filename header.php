<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Landmark National Bank
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script type="text/javascript">
	// Redirect to HTTPS.
	if (window.location.protocol != "https:"){
		window.location.href = "https:" + window.location.href.substring(window.location.protocol.length);
	}
</script>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" type="image/x-icon" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/min/respond.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/bower_components/rem-unit-polyfill/js/rem.min.js"></script>
<![endif]-->

<?php if( is_page(2296) || is_singular( 'loan_officer' ) ) : ?>
	<meta name="robots" content="noindex, nofollow">
<?php endif; ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">
<noscript>
	<div class="noscript">
		<?php _e( 'This site requires Javascript to function properly. Please enable Javascript and reload the page.', 'lnb' ); ?>
	</div>
</noscript>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content' ); ?></a>

	<input type="checkbox" id="nav-trigger" class="nav-trigger" />

	<div class="wrap header clearfix">

		<header id="masthead" class="site-header" role="banner" itemscope itemtype="https://schema.org/WPHeader">
			<div class="site-branding">
				<h1 class="site-title clearfix" itemprop="headline"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="clearfix"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Landmark National Bank" /></a></h1>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" aria-label="Main navigation">
				<form id="header-search" itemprop="potentialAction" itemscope itemtype="https://schema.org/SearchAction" method="GET" action="/" role="search">
					<!--[if lte IE 9]>
					Search
					<![endif]-->
					<input itemprop="query-input" type="text" name="s" placeholder="<?php _e( 'Search', 'lnb' ); ?>" class="clearfix" /><input type="submit" value="<?php _e( 'Go', 'lnb' ); ?>" />
				</form>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container_class' => 'menu-primary-container clearfix' ) ); ?>
			</nav><!-- #site-navigation -->

			<form action="https://pib.secure-banking.com/60366001/PassmarkSignIn.faces" method="POST" enctype="application/x-www-form-urlencoded" autocomplete="off" id="remote-login" class="clearfix">
				<div class="inside clear">
					<div class="account-login-header"><?php _e( 'Account Login', 'lnb' ); ?></div>
					<input type="hidden" name="remoteLogin" value="yes" />
					<div class="account-type-wrap">
					<div class="personal">
						<div class="label">Personal</div>
						<a title="Personal Account Login" href="https://web15.secureinternetbank.com/pbi_pbi1151/login/301171007" class="outgoing enroll-link">Login</a>
						<a href="https://web15.secureinternetbank.com/pbi_pbi1151/enroll/301171007" class="outgoing enroll-link">Enroll</a>
					</div>
					<div class="business">
						<div class="label">Business</div>
						<a title="Business Account Login" href="https://web15.secureinternetbank.com/ebc_ebc1151/Login/301171007/225" class="outgoing enroll-link">Login</a>
					</div>
					</div>
					<!-- <a href="https://pib.secure-banking.com/60366001/AutoEnrollmentIdentify.faces" class="outgoing enroll-link"><?php _e( 'Enroll', 'lnb' ); ?></a> -->
				</div>
			</form>

			<?php the_widget('Landmark_Quick_Links'); ?>
			
		</header><!-- #masthead -->

	</div><!-- .wrap -->

	<div class="mobile-header">
		<label for="nav-trigger"><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' x='0px' y='0px' width='50px' height='30px' viewBox='0 0 30 30' enable-background='new 0 0 30 30' xml:space='preserve'><rect width='50' height='6' /><rect y='24' width='50' height='6' /><rect y='12' width='50' height='6'/></svg></label>
		<h1 class="mobile-logo clearfix" aria="hidden" itemprop="headline"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="clearfix"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Landmark National Bank" /></a></h1>
	</div>
		


	<div class="canvas-wrap">

		<div class="wrap content clearfix">

		<?php if ( is_front_page() ) : ?>

			<?php include 'home-hero.php'; ?>

		<?php endif; ?>
			
			<div id="content" class="site-content">
