<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Landmark National Bank
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main clearfix">

			<section class="spotlight clearfix" aria-label="Spotlight content">

				<div class="description clearfix">
					<h2><span><?php the_field( 'home_spotlight_title' ); ?></span></h2>
					<?php the_field( 'home_spotlgiht_description' ); ?>
				</div>

				<aside class="spotlight-media">
					<?php 
						if ( 'video' == get_field( 'home_spotlight_media' ) ) :
							the_field('home_spotlight_video' );
						elseif ( 'image' == get_field( 'home_spotlight_media' ) ) :
							$spotlight_image = get_field('home_spotlight_image' );
							echo "<img src='{$spotlight_image['sizes']['large']}' alt='{$spotlight_image['alt']}' />";
						elseif ( 'carousel' == get_field( 'home_spotlight_media' ) ) :
							$spotlight_items = get_field('home_spotlight_carousel');
							echo "<div class='spotlight-carousel-items'>";
							foreach ( $spotlight_items as $item ){
								echo "<div class='spotlight-item-wrapper'>" . $item['embed'] ."</div>";
							}
							echo "</div>";
						endif; 
					?>
				</aside>

			</section>	

		</main><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript">

	jQuery(document).ready(function( $ ) {
	 
	  // $("#owl-carousel").owlCarousel({
	  // 		navigation : false, // Show next and prev buttons
	  // 		slideSpeed : 500,
	  // 		paginationSpeed : 400,
	  // 		singleItem : true,
	  // 		autoPlay : true,
	  // 		rewindSpeed : 2000
	  // });
	  
	  $('#slick-carousel').slick({
	  	autoplay: true,
	  	autoplaySpeed: 7000,
	  	speed: 800,
	  	pauseOnHover: false,
	  	dots: true,
	  	infinite: true
	  });

	  $('.spotlight-carousel-items').slick({
	  	autoplay: true,
	  	autoplaySpeed: 7000,
	  	speed: 800,
	  	pauseOnHover: false,
	  	dots: true,
	  	infinite: true
	  });
	 
	});

	</script>

<?php get_footer(); ?>
