<?php
/**
 * Template Name: Loan Officer Grid
 *
 * @package Landmark National Bank
 */

get_header(); ?>
	<?php
		// get all the loan officer terms
		$loc_terms = get_terms( 'region' );
	?>

	<section class="loan-officer-header">
		<div class="loan-officer-header__inner">
			<div class="loan-officer-header__inner--left">
				<h1>Leadership at Landmark</h1>
				<?php while( have_posts() ) { the_post(); the_content(); } ?>
			</div>

			<div class="loan-officer-header__inner--right">
				<h1>Join Our Team</h1>
				<a href="/careers/">See our current openings</a>
			</div>
		</div>
	</section> <!-- /.[loan-officer-header] -->

	<section class="loan-officer-main">
		<div class="grid-container">
			<?php // get all the terms and loop ?>
			<?php foreach( $loc_terms as $single_term ) : ?>

				<?php // get all the posts for each term ?>
				<?php $posts = new WP_Query( array(
					'post_type' => 'loan_officer',
					'tax_query' => array( array( 'taxonomy' => 'region', 'field' => 'slug', 'terms' => $single_term->slug ) ),
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'title',
					) );
				?>

				<h1><?php echo $single_term->name; ?></h1>
				<div class="loan-officer-grid">

				<?php // loop through all the posts in each term (category) ?>
				<?php while( $posts->have_posts() ) : $posts->the_post(); ?>
				
					<div class="loan-officer-grid__card">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo get_field( 'employee_image', $post->ID )['url']; ?>" alt="<?php echo get_field( 'employee_image', $post->ID )['alt']; ?>">
						</a>
						<a href="<?php the_permalink(); ?>"><h3 class="emp-name"><?php the_title(); ?></h3></a>
						<p class="emp-location"><?php echo get_field( 'city' ); ?>, KS</p>
					</div>
				<?php endwhile; wp_reset_postdata(); ?>

				</div> <!-- /.[__grid] -->
			<?php endforeach; ?>
		</div>
	</section>

<?php get_footer(); ?>